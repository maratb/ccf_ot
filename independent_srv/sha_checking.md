This server takes the source code, compiles it and checks the sha256sum of the result against the artifact from the first server.

Create your sha_check Freestyle project

![](https://i.imgur.com/mF6Hga5.png)

![](https://i.imgur.com/GZZXubp.png)

Here we assign the sha256sum values of our artifacts to two variables and compare them. If they are the same we run the deployment job, otherwise we run the deploy VM.
![](https://i.imgur.com/1tGMkfj.png)

```

TRUST=`sha256sum test_app`

cd /path/to/your/suspicious/app
SUSP=`sha256sum app`

if [ "$TRUST" = "$SUSP" ]; then curl http://username:11cc01a4c6edddf10db0021639d682cf59@localhost:8080/job/Project/job/deploy_app/build?token=ewfnwjkvjhgvjrgfethtbgfnfg; else curl http://username:11cc01a4c6edddf10db0021639d682cf59@localhost:8080/job/Project/job/vagrant_fp/build?token=ehgjdfhvjdfboethgoiteg; fi
echo "********** Checking Finished **********"
```
