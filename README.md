# Lab settings:

![](https://i.imgur.com/Xi0jY8P.png)

## In our work, we use Jenkins on both servers.

In this work, we split the CI / CD process into CI and CD. The first server (suspicious) 
produces the CI process. The entire path of the source code: pull, test, build and release. 

The second (independent), in turn, takes the same source code, compiles and compares its artifacts with 
those received from the first (suspicious) server. If the artifacts are identical, it continues the
deployment process, otherwise, the process of creating a virtual machine for the AppSec team is launched 
in order to use it as a sandbox for investigating malicious artifact.

- Use "Publish over ssh" plugin to send builded artifacts to local repository.
- Use freesyle project to build app and send artifact.
- Source code for compilation https://gitlab.com/maratb/test_c
- Source code for spoofing https://gitlab.com/maratb/pr



